#include "tb_driver.h"

void tb_driver::capture()
{
   /*
	retval = 0;
	// Wait for the start signal
	while (!start.read()) wait();
	//cout<<"Captur-Strt="<< start <<"     : " << sc_time_stamp() <<endl;
	wait();

	// Capture valid values only
	// non-valid leaves that address with default value (0 in systemc)
	wr_adr=0;
	for (int i=0;i<10;i++) {
		if (check) {
			res_sum[wr_adr] = data.read();
			wr_adr=wr_adr+1;
		}
		//cout<<"Captur(vld)="<< out_sum.read() << "(" << out_vld << ")  : " <<sc_time_stamp()<<endl;
		wait();
	}

	// Save the results to a file
	//cout << "File Open  : "<< sc_time_stamp()<<endl;
	ofstream result;
	result.open("result.dat");
	result << right << fixed << setbase(16) << setprecision(15);
	for (int j=0;j<wr_adr;j++) {
        result << setw(10) << j;
        result << setw(20) << res_sum[j];
        result << endl;
	}
	//cout << "File Wr  : "<< sc_time_stamp()<<endl;
	result.close();

	// Compare the results file with the golden results
	// (result message is displayed in the test bench)
	retval = system("diff --brief -w result.dat result.golden.dat");
	if (retval != 0) {
		retval=1;
	} 
    wait();
   */
}

void tb_driver::drive()
{
   unsigned times = 0;
   enum states { set_ready, check_if_valid, write_results, done };
   states state = set_ready;
   bool valid_get = true;
   sc_uint<2> state_get;

	while (!start.read()) wait();

	ofstream result;
	result.open("result.dat");
	result << right << fixed << setbase(16) << setprecision(15);

   while(1)
   {
	  state_get = state_in->read();
	  std::cout << "\nSTATE: " << state_get << std::endl << std::endl;
      switch(state)
      {
         case set_ready :
         {
            std::cout << "Set ready" << std::endl;
            //start->write(1);
            ready->write(1);
            state = check_if_valid;
            done_out->write(0);

            break;
         }
         case check_if_valid :
         {
            std::cout << "check_if_valid" << std::endl;
            valid_get = valid_in->read();

            if ( valid_get )
            {
               std::cout << "valid!!!" << std::endl;
               ready->write(0);
               ++times;
               res_sum[wr_adr] = request->read();
               std::cout << res_sum[wr_adr] << std::endl;
               wr_adr=wr_adr+1;
               done_out->write(1);

               if ( times == 10 )
               {
                  state = write_results;
               }
            }
            else
            {
               done_out->write(0);
            }

            break;
         }
         case write_results :
         {
            std::cout << "write_results" << std::endl;
            ofstream result;
            result.open("result.dat");
            //result << right << fixed << setbase(16) << setprecision(15);
            result << right << fixed;
            for (int j=0;j<wr_adr;j++) {
               std::cout << "RES to file: " << res_sum[j] << std::endl;
                 result << setw(10) << j;
                 result << "\t";
                 result << std::bitset<32>(res_sum[j]);
                 //result << setw(20) << res_sum[j];
                 result << endl;
            }
            //cout << "File Wr  : "<< sc_time_stamp()<<endl;
            result.close();
            retval = system("diff --brief -w result.dat result.golden.dat");
            if (retval != 0) {
               retval=1;
            } 
            state = done;
            break;
         }
         done :
         {
            break;
         }
         default :
         {
            break;
         }
      }
      wait();
   }
/*
	// Wait for start signal
	while (!start.read()) wait();
	//cout<<"Driver-Strt:"<< start <<"     : " << sc_time_stamp() <<endl;
	//cout<< " " <<endl;

	// Apply inputs, every 2nd input is valid
	for(int k=0;k<100; k++) {
		dat_a.write(k);
		dat_en = !dat_en;
		wait();
		//cout<<"Driver Enab="<< dat_en <<"     : " << sc_time_stamp() <<endl;
		//cout<<"Driver Data="<< dat_a <<"     : " << sc_time_stamp() <<endl;
		//cout<< " " <<endl;
	}
*/
}

// XSIP watermark, do not delete 67d7842dbbe25473c3c32b93c0da8047785f30d78e8a024de1b57352245f9689
