#include "request_generator.h"

void request_generator::done_check()
{
   bool done_get = 0;

   while (true)
   {
      wait();
      if ( done_signal_got )
      {
         done_get = 0;
      }
      else if ( done_in )
      {
         done_get = 1;
      }
      done_signal.write( done_get );
   }
}
void request_generator::free_check(  )
{
   bool free_get = 0;
   bool free_got = 0;

   while (true)
   {
      wait();
      if ( free_signal_got )
      {
         free_got = 1;
      }
      else
      {
         free_got = 0;
      }
      if ( free )
      {
         free_get = 1;
      }
      if ( free_got )
      {
         free_signal.write(0);
         free_get = 0;
      }
      else if ( free_get )
      {
         free_signal.write(1);
      }
      else
      {
         free_signal.write(0);
      }
   }
}
void request_generator::generator_thread()
{
#pragma HLS INTERFACE ap_none port=free bundle=free
   sc_uint<32> done_get = 0;                                              //to get done from done_signal
   bool read_free;                                                 //read parser state
   bool read_req;
   bool first_read = true;
   DT num_of_mems;
   DT cur_mem;
   DT base_seq_get = 0;
   DT base_skip_get = 0;
   DT cur_req_no;
   DT req_part = 0;
   DT tcp_size;
   DT req_size;
   DT req_size_tmp;
   DT fit_key;
   DT last_word;
   DT num_tmp;
   DT num_final;
   sc_uint<32> key_format;
   sc_uint<16> key_count = 0;
   sc_uint<32> key_cur;
   sc_uint<32> key_count_sort;
   unsigned stream_step;
   unsigned times;

   enum states { start_pair, handle_req, handle_read_req, to_ascii, to_ascii_epi, check_if_free, check_if_read_free, done_check, done_read_check, streaming, streaming_read, done };   //states of FSM

   //Initialization


   states state = start_pair;                                         //initialize state
   enable_out->write(0);
   valid_req->write(0);
   req_out->write(0);

   done_signal_got.write(0);
   free_signal_got.write(0);
   num_of_mems = num_mem->read();
   base_seq_get = sequence_start->read();
   base_skip_get = sequence_skip->read();

   state_out->write(-1);
   wait();

   cur_mem = 0;
   cur_req_no = 0;
   key_count = base_seq_get;

   while (!start.read()) wait();                                   //wait for DDR to be calibrated and TCP tp be ready
#ifndef __SYNTHESIS__
               cout << "\tDONE START" << endl;
#endif
   while(1)
   {
      wait();
      state_out->write(state);

      switch(state)
      {
         case start_pair :                                                            //get first word from a TCP_IN BRAM, starting with the first one
         {
#ifndef __SYNTHESIS__
               cout << "\tSTART PAIR" << endl;
#endif
        	 if ( cur_mem < num_of_mems )
        	 {
        		//fit_key = 0x78787878;
        		fit_key = 0;
        		read_req = false;
        	    state = to_ascii;
        	 }
        	 else if ( cur_mem < 2 * num_of_mems )
        	 {
        		if ( first_read )
        		{
        			first_read = false;
        			key_count = base_seq_get;
        			//cur_req_no = base_seq_get;
        		}
        		fit_key = 0;
        		read_req = true;
        	    state = to_ascii;
        	 }
        	 else
        	 {
        	    state = done;
        	 }
        	 key_cur = key_count;

             num_final = 0;
             times = 0;
             stream_step = 0;

        	 break;
         }
         case to_ascii:
         {
#ifndef __SYNTHESIS__
               cout << "\tTO ASCII" << endl;
#endif
        	 num_tmp = key_cur % 10;
        	 ++times;
        	 num_final |= num_tmp + 0x30;
        	 key_cur /= 10;


        	 if (key_cur == 0)
        	 {
        	    state = to_ascii_epi;
        	 }
        	 else
        	 {
        		 num_final <<= 8;
        	 }
             key_format = times + 8;
        	 break;
         }
         case to_ascii_epi:
         {
        	 if ( read_req )
        	 {
        		 tcp_size = (32 + times) << 16; //32 is data, times contains the numeric size of the key (for example, for key 23, times is 2) and 8 is the size of 'memtier-'
        		 req_size_tmp = 8 + times;
        		 fit_key = num_final;

        		 state = handle_read_req;
        	 }
        	 else
        	 {
        		 tcp_size = (64 + times + 8) << 16;
        		 req_size_tmp = 40 + times + 8;
        		 last_word = fit_key >> ((4 - times)<<3);

        		 state = handle_req;
        	 }
        	 tcp_size |= cur_req_no;
#ifndef __SYNTHESIS__
               cout << "\tFIT_KEY = " << fit_key << endl;
#endif
        	 key_count_sort.range(31,24) = key_format.range(7,0);
        	 key_count_sort.range(23,16) = key_format.range(15,8);
        	 req_size.range(31,24) = req_size_tmp.range(7,0);
        	 req_size.range(23,16) = req_size_tmp.range(15,8);
        	 req_size.range(15,8) = req_size_tmp.range(23,16);
        	 req_size.range(7,0) = req_size_tmp.range(31,24);

        	 break;
         }
         case handle_req :                                            //get extra length
		 {
			fit_key <<= (times<<3);
            req_out -> write( tcp_size );

            enable_out->write(1);
            valid_req->write( 1 );                           //valid for two cycles (make sure arbiter gets it)
            wait();
            enable_out->write(0);
            valid_req->write( 1 );                           //valid for two cycles (make sure arbiter gets it)
            wait();
            valid_req->write( 0 );

            state = check_if_free;

         	break;
         }
         case check_if_free :                                   //Parser available?
         {

            read_free = free_signal;

            if ( read_free )
            {
               free_signal_got.write(1);
			   wait();
               free_signal_got.write(0);

               state = done_check;
            }
            break;
         }
         case done_check :                                                     // check if parser is done with the word sent to it
         {
            done_get = done_signal.read();
            wait();
            if ( done_get )
            {
               done_signal_got.write(1);
               wait();
               wait();
               done_signal_got.write(0);

               if ( stream_step == 0)
               {
                  req_part = 0x00000180 | key_count_sort;
               	  state = streaming;
               }
               else if ( stream_step == 1)
               {
                  req_part = 0x00000008;
               	  state = streaming;
               }
               else if ( stream_step == 2 )
               {
                  req_part = req_size;
               	  state = streaming;
               }
               else if ( stream_step > 2 && stream_step < 8 )
               {
                  req_part = 0x00000000;
               	  state = streaming;
               }
               else if ( stream_step == 8 )
               {
                  req_part = 0x746d656d;
                  fit_key |= num_final;

               	  state = streaming;
               }
               else if ( stream_step == 9 )
               {
                  req_part = 0x2d726569;

               	  state = streaming;
               }
               else if ( stream_step == 10 )
               {
                  req_part = fit_key;
               	  state = streaming;
               }
               else if ( stream_step > 10 && stream_step < 18)
               {
                  //req_part = 0x78787878;
            	   req_part = cur_req_no;
               	  state = streaming;
               }
               else if ( stream_step == 18 )
               {
                  req_part = last_word;
               	  state = streaming;
               }
               else
               {
                  ++cur_mem;
                  ++cur_req_no;
                  key_count += base_skip_get;
                  state = start_pair;
               }
            }

            break;
         }
         case streaming :                                          //send the rest of the words
         {
        	   ++stream_step;
               req_out -> write( req_part );
               valid_req->write( 1 );
               wait();
               wait();
               valid_req->write( 0 );
               ++req_part;
               state = done_check;

            break;
         }
         case handle_read_req :                                            //get extra length
		 {

            req_out -> write( tcp_size );

            enable_out->write(1);
            valid_req->write( 1 );                           //valid for two cycles (make sure arbiter gets it)
            wait();
            enable_out->write(0);
            valid_req->write( 1 );                           //valid for two cycles (make sure arbiter gets it)
            wait();
            valid_req->write( 0 );

            state = check_if_read_free;

         	break;
         }
         case check_if_read_free :                                   //Parser available?
         {
            read_free = free_signal;

            if ( read_free )
            {
               free_signal_got.write(1);
			   wait();
               free_signal_got.write(0);

               state = done_read_check;
            }
            break;
         }
         case done_read_check :                                                     // check if parser is done with the word sent to it
         {
            done_get = done_signal.read();
            wait();
            if ( done_get )
            {
               done_signal_got.write(1);
               wait();
               wait();
               done_signal_got.write(0);

               if ( stream_step == 0 )
               {
            	   req_part = 0x00000080 | key_count_sort;
            	   state = streaming_read;
               }
               else if ( stream_step == 1 )
               {
                  req_part = 0x00000000;
               	  state = streaming_read;
               }
               else if ( stream_step == 2 )
               {
                  req_part = req_size;
               	  state = streaming_read;
               }
               else if ( stream_step > 2 && stream_step < 6 )
               {
                  req_part = 0x00000000;
               	  state = streaming_read;
               }
               else if ( stream_step == 6 )
               {
                  req_part = 0x746d656d;
               	  state = streaming_read;
               }
               else if ( stream_step == 7 )
               {
                  req_part = 0x2d726569;
               	  state = streaming_read;
               }
               else if ( stream_step == 8 )
               {
                  req_part = fit_key;
               	  state = streaming_read;
               }
               else
               {
                  ++cur_mem;
                  ++cur_req_no;
                  key_count += base_skip_get;
                  state = start_pair;
               }
            }

            break;
         }
         case streaming_read :                                          //send the rest of the words
         {
        	 ++stream_step;
        	 req_out -> write( req_part );
        	 valid_req->write( 1 );
        	 wait();
        	 wait();
        	 valid_req->write( 0 );
        	 ++req_part;
        	 state = done_read_check;

        	 break;
         }
         case done :                 //I'm done
         {
            break;
         }
         default :
         {
            break;
         }
      }
   }
}
