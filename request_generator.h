#ifndef REQUEST_GENERATOR_H
#define REQUEST_GENERATOR_H

// to enable fixed-poi
#define SC_INCLUDE_FX
#include <systemc.h>
#include <tlm.h>
#include "length_config.h"
#include "AXI4_if.h"

using namespace tlm;

#define DT sc_uint<32>

SC_MODULE(request_generator){
	//Ports
   sc_in<bool> clock; //clock input
   sc_in<bool> reset;
   sc_in<bool> free;
   sc_in<bool> start;
   sc_in<bool> done_in;
   sc_in<DT > num_mem;
   sc_in<DT > sequence_start;
   sc_in<DT > sequence_skip;

   //sc_out<bool> done_out;
   sc_out<bool> enable_out;
   sc_out<sc_uint<OP_L> > state_out;
   sc_out<bool> valid_req;
   sc_out<sc_uint<32> > req_out;

   //Variables
   sc_signal<bool> done_signal;
   sc_signal<bool> done_signal_got;
   sc_signal<bool> free_signal;
   sc_signal<bool> free_signal_got;

   //Process Declaration
   void generator_thread();
   void done_check();
   void free_check();
	//Constructor
	SC_CTOR(request_generator)
   {
#pragma HLS INTERFACE ap_none port=free
		//Process Registration
		SC_CTHREAD(generator_thread,clock.pos());
		reset_signal_is(reset,true);
		SC_CTHREAD(done_check,clock.pos());
		reset_signal_is(reset,true);
		SC_CTHREAD(free_check,clock.pos());
		reset_signal_is(reset,true);
	}

};

#endif
