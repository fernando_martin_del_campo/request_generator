#include "tb_init.h"

void tb_init::prc_reset() {
	reset = true;
	start = false;
	cons = 5;
	wait(10,SC_NS);

	reset = false;
	wait(10,SC_NS);
	wait(10,SC_NS);
	//cout<<"Reset Off  ="<< reset <<" : "<< sc_time_stamp()<<endl;

	start = true;
	wait(10,SC_NS);
	//cout<<"Start On   ="<< start <<" : "<< sc_time_stamp()<<endl;
	//cout<< " " <<endl;
};
