#include <systemc.h>
#include "tlm.h"
using namespace tlm;

#include "hls_bus_if.h"
#ifdef __RTL_SIMULATION__
#include "request_generator_rtl_wrapper.h"
#define request_generator request_generator_rtl_wrapper
#else
#include "request_generator.h"
#endif

#include "tb_init.h"
#include "tb_driver.h"

int sc_main (int argc , char *argv[]) 
{
	// Remove simulation warnings
	sc_report_handler::set_actions("/IEEE_Std_1666/deprecated", SC_DO_NOTHING);
	sc_report_handler::set_actions( SC_ID_LOGIC_X_TO_BOOL_, SC_LOG);
	sc_report_handler::set_actions( SC_ID_VECTOR_CONTAINS_LOGIC_VALUE_, SC_LOG);
	sc_report_handler::set_actions( SC_ID_OBJECT_EXISTS_, SC_LOG);
	// Test bench signals
	sc_signal<bool>	s_reset;
	sc_signal<bool>	s_start;
	sc_signal<bool>	s_free;
	sc_signal<bool>	s_done;
	sc_signal<bool>	s_enable;
	sc_signal<bool>	s_valid;
	sc_signal<sc_uint<OP_L> >	s_state;
	sc_signal<sc_uint<32> >	s_data;
	sc_signal<sc_uint<32> >	s_cons;

   hls_bus_chn<sc_uint<32> > bus_mem("bus_mem",0,1024);

	sc_clock s_clk("s_clk",10,SC_NS);       // Create a 10ns period clock signal

	// Test bench modules
	tb_init	U_tb_init("U_tb_init");
	request_generator	U_dut("U_dut");
	tb_driver	U_tb_driver("U_tb_driver");
	 
	// Generate a reset & start signal to drive the sim
	U_tb_init.clk(s_clk);
	U_tb_init.reset(s_reset);
	U_tb_init.cons(s_cons);
	U_tb_init.start(s_start);

	// Connect the DUT
	U_dut.clock(s_clk);
	U_dut.reset(s_reset);
	U_dut.free(s_free);
	U_dut.start(s_start);
	U_dut.done_in(s_done);
	U_dut.num_mem(s_cons);

	U_dut.enable_out(s_enable);
	U_dut.valid_req(s_valid);
	U_dut.state_out(s_state);
	U_dut.req_out(s_data);

	// Drive stimuli & Capture results
	U_tb_driver.clk(s_clk);
	U_tb_driver.reset(s_reset);
	U_tb_driver.enable_in(s_enable);
	U_tb_driver.valid_in(s_valid);
	U_tb_driver.start(s_start);
	U_tb_driver.request(s_data);
	U_tb_driver.ready(s_free);
	U_tb_driver.state_in(s_state);
	U_tb_driver.done_out(s_done);
   
	// Sim 
	int end_time = 10000;
  
	cout << "INFO: Simulating " << endl;
	
	// start simulation 
	sc_start(end_time, SC_NS);

	// Print summary of results check
	if (U_tb_driver.retval != 0) {
		printf("Test failed  !!!\n"); 

	} else {
		printf("Test passed !\n");
  }
  return U_tb_driver.retval;

};

