Request generator
=====

Generates stripped down Memcached synthetic requests.

Build
---

IMPORTANT: Build this module in Vivado HLS 2018.3 or 2019.X.
```
vivado_hls -f run_hls.tcl
```

Open project
---

```
vivado_hls -p proj_req_generator
```
