#ifndef TB_DRIVER_H
#define TB_DRIVER_H

#include <fstream>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <bitset>
#include "length_config.h"
using namespace std;

#include <systemc.h>

SC_MODULE(tb_driver)
{
	//Ports
  sc_in_clk    clk;
  sc_in <bool> reset;
  sc_in <bool> enable_in;
  sc_in <bool> start;
  sc_in <bool> valid_in;

  sc_in<sc_uint<OP_L> > state_in;
  sc_in<sc_uint<32> > request;

  sc_out <bool> ready;
  sc_out <bool> done_out;
	//Variables
  bool retval;
  sc_uint<32> res_sum[10];
  unsigned  wr_adr;

	//Process Declaration
  void drive();
  void capture();

	//Constructor
  SC_CTOR(tb_driver)
  {
		// Input Driver process
    SC_CTHREAD(drive,clk.pos());
    reset_signal_is(reset,true);

		// Output Capture process
    SC_CTHREAD(capture,clk.pos());
    reset_signal_is(reset,true);
  }
};

#endif
