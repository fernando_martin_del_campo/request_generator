#ifndef TB_INIT_H
#define TB_INIT_H

#include <systemc.h>
#include <iostream>
#include "string"
using namespace std;

SC_MODULE(tb_init)
{
	//Ports
	sc_in<bool> clk;
	sc_out<bool> reset;
	sc_out<bool> start;
	sc_out<sc_uint<32>> cons;

	//Process Declaration
	void prc_reset();

	//Constructor
  SC_CTOR(tb_init)
	{
		//Process Registration
    SC_CTHREAD(prc_reset,clk.pos());
	}
		
};

#endif
